const 
    express = require('express'),
    ews = require('express-ws'),
    AlarmServer = require('./AlarmServer');


var config = require('./config');

const app = express();
ews(app);

const router = express.Router();

const service = new AlarmServer(config);
service.register(router);

app.use(config.basePath, router);
app.listen(config.port);
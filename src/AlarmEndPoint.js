// @ts-check

const { connect } = require('http2');
const { connected, disconnect } = require('process');

const
  { forEach } = require('lodash'),
  debug = require('debug')('alarm:endpoint');

  const WS_GOING_AWAY = 1001;
  const WS_INTERNAL_ERROR = 1011;
  

class AlarmEndPoint {

    constructor() {
        this._id = 0;
        this._clients = {};
        this.value = null;
    }

    dispatch(data) {
        this.value = data;
        try {
            let jsonPayload = JSON.stringify(data);
            forEach(this._clients, function(cb) { cb(undefined, jsonPayload); });
        }catch(error){
            this.dispatchError(error);
        }
    }

    dispatchError(err) {
        debug("Endpoint error " + err);
        this.value = [{  class: "Alarm gateway", 
                        date : Date.now(),
                        ident : "AlarmGw/Internal#NotConnected",
                        severity : 2,
                        text : "Disconnected from alarm server"
                    }];
        forEach(this._clients, (cb) => cb(this.value));
    }
    

    release() {
        forEach(this._clients, (cb) => cb());
    }

    onConnect(cb) {
        const id = this._id++;
        this._clients[id] = cb;
        return id;
    }

    onDisconnect(id) {
        delete this._clients[id];
    }

    register(router, path) {
        router.get(path, (req, res) => {
            if(this.value) {
                res.json(this.value);
            }else{
                res.json([{  class: "Alarm gateway", 
                    date : Date.now(),
                    ident : "AlarmGw/Internal#NotConnected",
                    severity : 2,
                    text : "Disconnected from alarm server"
                }]);
            }           
          });
      
        if (router.ws) {
            (router).ws(path, (ws) => {
                try {
                    const id = this.onConnect((err, data) => {
                    if (err) {
                        ws.send(JSON.stringify(err));
                    }
                    else if (data !== undefined) {
                        ws.send(data);
                    }
                    else {
                        ws.close(WS_GOING_AWAY);
                    }
                    });
                    if (this.value !== null) {
                        ws.send(this.value);
                    }
                    ws.once('close', () => {
                        this.onDisconnect(id);
                    });
                }
                catch (err) {
                    ws.close(WS_INTERNAL_ERROR, err.message);
                }
            });
        }
    }
}

module.exports = AlarmEndPoint;
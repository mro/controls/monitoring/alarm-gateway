
const
    { get, has, throttle, isEmpty, forEach, toString } = require('lodash'),
    AlarmEndPoint = require('./AlarmEndPoint'),
    zmq = require('zeromq'),
    debug = require('debug')('alarm:zmqendpoint'),
    Watchdog = require('./Watchdog');


class ZMQEndPoint extends AlarmEndPoint {

    constructor(config) {
        super();
        this._connected = true;
        this._socket = null;
        this._endPoint = `tcp://${get(config, [ 'host' ])}:${get(config, [ 'port' ])}`;
        this._wd = new Watchdog(10);
        this._wd.on('expired', this._onWatchdogExpired.bind(this));
        this._connect();
    }


    _connect() {
        debug(`Connecting to ${this._endPoint}`);
        this._socket = zmq.socket("sub");
        this._socket.monitor();
        this._socket.connect(this._endPoint);
        this._socket.subscribe('Alarm');
        this._socket.on('message', this._msgReceived.bind(this));
        this._socket.on('connect', function(v, a, e) {this._connected = true});
        this._socket.on('close', this._errorReceived.bind(this));
        this._socket.on('connect_retry', this._errorReceived.bind(this));
    }

    _disconnect() {
        if (this._connected) {
            debug(`Disconnecting from ${this._endPoint}`);
            this._socket.disconnect();
            this._connected = false;
        }
    }

    _msgReceived(topic, message) {
        try{
            let json = JSON.parse(message.toString());
            super.dispatch(json);
        }catch(err) {
            debug(`Unable to parse JSON : ${err}`);
        }
    }

    _errorReceived(eventValue, eventEndpointAddrress, error) {
        super.dispatchError(error);
    }

    _onWatchdogExpired() {
        debug('error watchdog triggered, reconnecting');
        this._disconnect();
        this._connect();
        /* restart watchdog, will be cleared by first correct message */
        this.wd.start();
    }

    release() {
        debug(`Releasing ${this.host}:${this.port}`);
        this._disconnect();        
        this.wd.reset();
        super.release();
    }
}

module.exports = ZMQEndPoint;
const { defaultTo } = require('lodash');

var config;
try {
  // @ts-ignore
  config = require('/etc/app/config'); /* eslint-disable-line global-require */
}
catch (e) {
  // @ts-ignore
  config = require('./config-stub'); /* eslint-disable-line global-require */
}

config.port = defaultTo(config.port, 8080);
config.basePath = defaultTo(config.basePath, '');

module.exports = config;
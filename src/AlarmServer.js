
const
    { get, forEach } = require('lodash');
const
    debug = require('debug')('alarm:server'),
    TCPEndPoint = require('./TCPEndPoint'),
    ZMQEndPoint = require('./ZMQEndPoint');

class AlarmServer {

    constructor(config) {
        this.config = config;
        /** @type {{ [path: string]: AlarmEndpoint }} */
        this.endpoints = {};
    }
    
    register(router) {
        this.release();
        forEach(this.config.endpoints,
          (config) => this.registerAlarmServer(router, config));
        return router;
    }
    
    registerAlarmServer(router, config) {
        const path = get(config, [ 'path' ], `/${config.subsystem}`);
        const mode = get(config, [ 'mode' ], 'TCP');
        var ep = null;
        if(mode == 'TCP') {
            ep = new TCPEndPoint(config);
            debug(`Creating a new TCP endpoint at ${path}`);
        }else if(mode == 'ZMQ') {
            ep = new ZMQEndPoint(config);
            debug(`Creating a new ZMQ endpoint at ${path}`);
        }

        ep.register(router, path);
        this.endpoints[path] = ep;

    }

    release() {
        forEach(this.endpoints, (ep) => {
          ep.release();
        });
        this.endpoints = {};
    }
    
}
module.exports = AlarmServer;
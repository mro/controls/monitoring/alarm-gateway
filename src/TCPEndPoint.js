// @ts-check


const
  { get, has, throttle, isEmpty, forEach, toString } = require('lodash'),
  AlarmEndPoint = require('./AlarmEndPoint'),
  net = require('net'),
  debug = require('debug')('alarm:tcpendpoint'),
  Watchdog = require('./Watchdog');

class TCPEndPoint extends AlarmEndPoint {

    constructor(config) {
        super();
        this.host = get(config, [ 'host' ]);
        this.port = get(config, [ 'port' ]);
        this.config = config;
        this._connected = false;
        this._client = null;        
        this.wd = new Watchdog(10);
        this.wd.on('expired', this._onWatchdogExpired.bind(this));
        this._connect();
    }

    _connect() {
        debug(`Connecting to ${this.host}:${this.port}`);
        this.client = new net.Socket();
        this.client.on('data', this._cummulateData.bind(this));
        this.client.on('error', super.dispatchError.bind(this));
        this.client.on('connect', (function(){ this._connected = true; }).bind(this));
        this.client.on('close', super.dispatchError.bind(this));
        this._msgBuffer = null;
        this._msgLength = -1;
        this.client.connect(this.port, this.host);
        this._connected = true;
    }

    _disconnect() {
        if (this._connected) {
            debug(`Disconnecting from ${this.host}:${this.port}`);
            this.client.destroy();
            this._connected = false;
        }
    }

    _cummulateData(data) {
        if(this._msgBuffer){
            this._msgBuffer = Buffer.concat([this._msgBuffer, data]);
        }else{
            this._msgBuffer = data;
        }
        if(this._msgLength <= 0) {
            let endPlace = this._msgBuffer.indexOf('\n');
            if(endPlace > 0){
                let lenStr = this._msgBuffer.slice(0, endPlace);
                this._msgLength = parseInt(lenStr, 10);
                this._msgBuffer = this._msgBuffer.slice(endPlace+1);
            }
        }
        debug(`Read ${this._msgBuffer.length}/${this._msgLength} bytes`);
        if(this._msgBuffer.length >= this._msgLength){
            let data = this._msgBuffer.slice(0, this._msgLength);
            try{
                let json = JSON.parse(data.toString());
                super.dispatch(json);
            }catch(err) {
                debug(`Unable to parse JSON : ${err}`);
            }
            this._msgBuffer = this._msgBuffer.slice(this._msgLength);
            this._msgLength = -1;
            debug(`Dispatched new message`);
        }
    }
    
    _onWatchdogExpired() {
        debug('error watchdog triggered, reconnecting');
        this._disconnect();
        this._connect();
        /* restart watchdog, will be cleared by first correct message */
        this.wd.start();
    }

    release() {
        debug(`Releasing ${this.host}:${this.port}`);
        this._disconnect();        
        this.wd.reset();
        super.release();
    }
}

module.exports = TCPEndPoint;